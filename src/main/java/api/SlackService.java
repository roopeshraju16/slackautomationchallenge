package api;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import models.ArchiveChannel;
import models.CreateChannel;
import models.JoinChannel;
import utils.ErrorMessage;
import utils.SlackApiPath;
import org.junit.Assert;

import static io.restassured.RestAssured.*;

public class SlackService {

    private final static String BASE_URI="https://slack.com";


    CreateChannel createChannel = new CreateChannel();

    JoinChannel joinChannel = new JoinChannel();

    ArchiveChannel archiveChannel = new ArchiveChannel();


    public Response accessToken(){
        Response accessToken;
        try{
            accessToken = given().header("Accept", "application/json")
                .header("Content-Type", "application/json").log().all().when()
                .get(BASE_URI + SlackApiPath.SLACK_GENERATE_TOKEN).then().assertThat()
                .statusCode(200).extract().response();
        }catch (Exception e){
            throw new RuntimeException(ErrorMessage.ERROR_GENERATE_OAUTH_TOKEN, e);
        }
        return accessToken;
    }


    public Response createChannel(String tokenValue,String channelName){
        Response createChannelResponse;
        try{
            createChannelResponse = given().header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Accept-Language","en").queryParam("scope","teams:roopesh")
                .queryParam("access_token", tokenValue).queryParam("name", channelName)
                .queryParam("pretty", 1).log().all().when()
                .get(BASE_URI + SlackApiPath.SLACK_CREATE_CHANNEL).then().assertThat()
                .statusCode(200).extract().response();
        }catch (Exception e){
            throw new RuntimeException(ErrorMessage.ERROR_CREATION_SLACK_CHANNEL, e);
        }
        return createChannelResponse;
    }


    public void validateCreationResponse(Response response,String expectedMessage,String expectedChannelName){
        try{
            JsonPath responseBody= response.getBody().jsonPath();
            createChannel.setOk(responseBody.getString("ok"));
            if(createChannel.getOk().equalsIgnoreCase("true")){
              createChannel.setName(responseBody.getString("channel.name"));
               org.junit.Assert.assertEquals("Channel Name mismatch", expectedChannelName,
                    createChannel.getName());
            }else {
             createChannel.setError(responseBody.getString("error"));
             Assert.assertEquals("Auth error message mismatch", expectedMessage,
                    createChannel.getError());
            }
        }catch (Exception e){
            throw new RuntimeException(ErrorMessage.ERROR_SETTING_UP_RESPONSE, e);
        }
    }



    public Response joinChannel(String tokenValue,String channelName){
        Response createChannelResponse;
        try{
            createChannelResponse = given().header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Accept-Language","en").queryParam("scope","teams:roopesh")
                .queryParam("access_token", tokenValue).queryParam("name", channelName)
                .queryParam("pretty", 1).log().all().when()
                .get(BASE_URI + SlackApiPath.SLACK_JOIN_CHANNEL).then().assertThat()
                .statusCode(200).extract().response();
        }catch (Exception e){
            throw new RuntimeException(ErrorMessage.ERROR_CREATION_SLACK_JOIN_CHANNEL, e);
        }
        return createChannelResponse;
    }


    public void validateJoinChannelResponse(Response response,boolean channelTrue,String error){
        try {
            JsonPath responseBody= response.getBody().jsonPath();
            joinChannel.setOk(responseBody.getString("ok"));
            if (joinChannel.getOk().equalsIgnoreCase("true")){
                joinChannel.setIs_channel(responseBody.getBoolean("channel.is_channel"));
                Assert.assertEquals("Joining in channel is unsuccess",channelTrue,joinChannel.isIs_channel());
            }else {
                joinChannel.setError(responseBody.getString("error"));
                Assert.assertEquals("Channel id Not found",joinChannel.getError(),error);
            }
        }catch (Exception e){
            throw new RuntimeException(ErrorMessage.ERROR_SETTING_JOIN_CHANNEL_RESPONSE, e);
        }
    }


    public Response renameChannel(String tokenValue,String channelRename,String channelName){
        Response renameChannel;
        try{
            renameChannel = given().header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Accept-Language","en").queryParam("scope","teams:roopesh")
                .queryParam("access_token", tokenValue).queryParam("name", channelName)
                .queryParam("channel", channelRename)
                .queryParam("pretty", 1).log().all().when()
                .get(BASE_URI + SlackApiPath.SLACK_CREATE_CHANNEL).then().assertThat()
                .statusCode(200).extract().response();
        }catch (Exception e){
           throw new RuntimeException(ErrorMessage.ERROR_SETTING_RENAME_CHANNEL,e);
        }
        return renameChannel;
    }


    public void validateRenameChannel(Response response,boolean channelTrue,String error,String ok){
        try {
            JsonPath responseBody= response.getBody().jsonPath();
            joinChannel.setOk(responseBody.getString("ok"));
            if (joinChannel.getOk().equalsIgnoreCase("true")){
                joinChannel.setIs_channel(responseBody.getBoolean("channel.is_channel"));
                Assert.assertEquals("renameing  in channel is unsuccess",channelTrue,joinChannel.isIs_channel());
            }else {
                joinChannel.setError(responseBody.getString("error"));
                Assert.assertEquals("renaming channel error code mismatch ",joinChannel.getError(),error);
            }
        }catch (Exception e){
            throw new RuntimeException(ErrorMessage.ERROR_SETTING_JOIN_CHANNEL_RESPONSE, e);
        }
    }


    public Response archiveChannel(String tokenValue,String channelName){
        Response archiveChannel;
        try{
            archiveChannel = given().header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .header("Accept-Language","en").queryParam("scope","teams:roopesh")
                .queryParam("access_token", tokenValue).queryParam("name", channelName)
                .queryParam("pretty", 1).log().all().when()
                .get(BASE_URI + SlackApiPath.SLACK_ARCHIVE_CHANNEL).then().assertThat()
                .statusCode(200).extract().response();
        }catch (Exception e){
            throw new RuntimeException(ErrorMessage.ERROR_SETTING_ARCHIVE_CHANNEL,e);
        }
        return archiveChannel;
    }

    public void validateArchiveChannel(Response response,String ok,String expectedErrorCode){
        try {
            JsonPath responseBody= response.getBody().jsonPath();
            archiveChannel.setOk(responseBody.getString("ok"));
            if (archiveChannel.getOk().equalsIgnoreCase("true")){
                Assert.assertEquals("archiving is unsuccess",ok,archiveChannel.getOk());
            }else {
                archiveChannel.setError(responseBody.getString("error"));
                Assert.assertEquals("Archiving error code mismatch", archiveChannel.getError(),
                    expectedErrorCode);
            }
        }catch (Exception e){
            throw new RuntimeException(ErrorMessage.ERROR_VALIDATING_ARCHIVE_CHANNEL,e);
        }
    }
}
