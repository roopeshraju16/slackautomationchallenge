package utils;

public class ErrorMessage {
    public static final String ERROR_GENERATE_OAUTH_TOKEN="Unable to generate the oauth token";

    public static final String ERROR_CREATION_SLACK_CHANNEL="Error in creation of channel in slack";

    public static final String ERROR_SETTING_UP_RESPONSE="Error in setting up the channel name response";

    public static final String ERROR_CREATION_SLACK_JOIN_CHANNEL="Error in joining the channel";

    public static final String ERROR_SETTING_JOIN_CHANNEL_RESPONSE="Error in setting up the channel response";

    public static final String ERROR_SETTING_RENAME_CHANNEL="Error in renaming the channel";

    public static final String ERROR_SETTING_ARCHIVE_CHANNEL="Error setting up the archive request";

    public static final String ERROR_VALIDATING_ARCHIVE_CHANNEL="Error in validating the archive response";
}
