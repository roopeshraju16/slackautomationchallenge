package utils;

public class SlackApiPath {
    public static final String SLACK_GENERATE_TOKEN = "/oauth/authorize";

    public static final String SLACK_CREATE_CHANNEL="/api/channels.create";

    public static final String SLACK_JOIN_CHANNEL="/api/channels.join";

    public static final String SLACK_RENAME_CHANNEL="/api/channels.rename";

    public static final String SLACK_ARCHIVE_CHANNEL="/api/channels.archive";
}
