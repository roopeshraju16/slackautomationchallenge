import cucumber.api.CucumberOptions;

import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "/Users/coviam/slackautomationchallenge/src/test/resources/features", tags = {"@Slack"}, plugin = {"pretty",
    "html:target/cucumber-html-report", "json:target/cucumber.json",
    "junit:target/cucumber.xml"}, glue = {"steps"})
public class CucumberRunner {

}
