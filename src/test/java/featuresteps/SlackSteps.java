package featuresteps;

import api.SlackService;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;

public class SlackSteps {
    SlackService slackService = new SlackService();

    Response creationChannelResponse, joinChannel, renameChannel, archiveChannel;

    @Given("^create the (.*) name$") public void createTheChannelName(String channelName) {
        slackService.createChannel("3307f436963e02d4f9eb85ce5159744c", channelName);
    }

    @Then("^verify whether (.*) is created$")
    public void verifyWhetherChannelIsCreated(String channelName) {
        slackService.validateCreationResponse(creationChannelResponse, "", channelName);
    }

    @Given("^provide the invalid token while creation of channel$")
    public void provideTheInvalidTokenWhileCreationOfChannel() {
        creationChannelResponse =
            slackService.createChannel("3307f436963e02d4f9eb85ce5159744c", "default");
    }

    @Then("^verify for the (.*)$")
    public void verifyForTheExpectedErrorMessage(String expectedErrorMessage) {
        slackService.validateCreationResponse(creationChannelResponse, expectedErrorMessage, "");
    }

    @Given("^join as a new user in the create channel$")
    public void joinAsANewUserInTheCreatecChannel() {
        joinChannel = slackService.joinChannel("3307f436963e02d4f9eb85ce5159744c", "default");
    }

    @Then("^verify with the list of users$") public void verifyWithTheListOfUsers() {
        slackService.validateJoinChannelResponse(joinChannel, true, "");
    }

    @Given("^rename the channel which is created$") public void renameTheChannelWhichIsCreated() {
        renameChannel = slackService.renameChannel("", "3rdFloor", "default");
    }

    @Then("^validate whether the channel name has changed succesfully$")
    public void validateWhetherTheChannelNameHasChangedSuccesfully() {
        slackService.validateRenameChannel(renameChannel, true, "", "true");
    }

    @Given("^Archive the channel which is created$") public void archiveTheChannelWhichIsCreated() {
        archiveChannel = slackService.archiveChannel("", "default");
    }

    @Then("^validate whether channel got archived$")
    public void validateWhetherChannelGotArchived() {
        slackService.validateArchiveChannel(archiveChannel, "true", "");
    }


}
