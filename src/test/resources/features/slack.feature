@Slack
Feature: The feature describes all about creating the channel in the slack and perform various actions

  Scenario Outline: Creation of new channel
    Given create the <channel> name
    Then verify whether <channel> is created
    Examples:
      | channel |
      | RE      |


  Scenario Outline: validate for the invalid auth while creation of new channel
    Given provide the invalid token while creation of channel
    Then verify for the <expectedErrorMessage>
    Examples:
      | expectedErrorMessage |
      | not_authed           |


  Scenario: Join the newly created channel
    Given join as a new user in the create channel
    Then verify with the list of users


  Scenario: Rename the channels
    Given rename the channel which is created
    Then validate whether the channel name has changed succesfully


  Scenario: Archive the created channel
    Given Archive the channel which is created
    Then validate whether channel got archived
